/**
 * Created by : Dhana Prasad
 * Class Name : RandomStringGenerator
 * Description : This class will help to generate a random string with length provided as input so that it can be used as one time password
 */
public with sharing class RandomStringGenerator {
    /**
 * Method Name : generateRandomString
 * Description : This method will help to generate a random string with length provided as input so that it can be used as one time password
 * Params : len (length of random string to be generated)
 */
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
}
